#! /bin/bash

rm -rf www
rm app.zip
ionic build --prod
zip -r app.zip www
scp app.zip fsquiroz:~/movements.zip
rm app.zip
