export const environment = {
  production: true,
  baseUrl: 'https://movements.fsquiroz.com',
  pageSize: 20,
  defaultDateFormat: 'yyyy/MM/dd HH:mm:ss',
  version: '1.2.1'
};
