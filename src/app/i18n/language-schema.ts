export class LanguageSchema {
  general: GeneralLanguageSchema;
  sort: SortLanguageSchema;
  login: LoginLanguageSchema;
  user: UserLanguageSchema;
  account: AccountLanguageSchema;
  movement: MovementLanguageSchema;
  permission: PermissionLanguageSchema;
  grouping: GroupingLanguageSchema;
  currency: CurrencyLanguageSchema;
  setting: SettingLanguageSchema;
  notFound: NotFoundLanguageSchema;
  toast: { [k: string]: string };
}

export class GeneralLanguageSchema {
  name: string;
  close: string;
  cancel: string;
  save: string;
  new: string;
  add: string;
  edit: string;
  delete: string;
  remove: string;
  select: string;
  registered: string;
  loading: string;
  amount: string;
  view: string;
}

export class SortLanguageSchema {
  title: string;
  none: string;
  creation: string;
  name: string;
  registered: string;
  description: string;
  debit: string;
  credit: string;
}

export class LoginLanguageSchema {
  title: string;
  email: string;
  password: string;
  submit: string;
}

export class UserLanguageSchema {
  singular: string;
  search: string;
}

export class AccountLanguageSchema {
  singular: string;
  plural: string;
  create: string;
  delete: string;
  leaveTitle: string;
  leaveAction: string;
  search: string;
  balance: string;
  detail: string;
  sharing: string;
  resume: string;
  isOwner: string;
  canEdit: string;
  canNotEdit: string;
}

export class MovementLanguageSchema {
  singular: string;
  plural: string;
  resume: string;
  creator: string;
  editor: string;
  search: string;
  debit: string;
  credit: string;
  noDesc: string;
  description: string;
  txType: string;
  newBalance: string;
  currentBalance: string;
  delete: string;
}

export class PermissionLanguageSchema {
  singular: string;
  plural: string;
  delete: string;
  level: string;
  read: string;
  write: string;
}

export class GroupingLanguageSchema {
  singular: string;
  plural: string;
  create: string;
  search: string;
  delete: string;
  detail: string;
  total: string;
  totals: string;
  noBalance: string;
  totalsByCcy: string;
}

export class CurrencyLanguageSchema {
  singular: string;
  plural: string;
  search: string;
}

export class SettingLanguageSchema {
  title: string;
  plural: string;
  dateFormat: string;
  defaultLanguage: string;
  updatePassword: string;
  oldPassword: string;
  newPassword: string;
  confirmPassword: string;
  mismatchPasswords: string;
  advance: string;
  currentVersion: string;
  currentServerVersion: string;
  update: string;
  updating: string;
  refreshTitle: string;
  refreshSubtitle: string;
}

export class NotFoundLanguageSchema {
  title: string;
  message: string;
  nav: string;
}
