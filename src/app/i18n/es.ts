import {LanguageSchema} from './language-schema';
import {TranslateService} from '@ngx-translate/core';

export class Es {

  private static readonly language: LanguageSchema = {
    general: {
      name: 'Nombre',
      close: 'Cerrar',
      cancel: 'Cancelar',
      save: 'Guardar',
      new: 'Nuevo',
      add: 'Agregar',
      edit: 'Editar',
      delete: 'Eliminar',
      remove: 'Quitar',
      select: 'Seleccionar',
      registered: 'Registrado',
      loading: 'Cargando',
      amount: 'Monto',
      view: 'Ver'
    },
    sort: {
      title: 'Ordenar por',
      none: 'Ninguno',
      creation: 'Creación',
      name: 'Nombre',
      registered: 'Registrado',
      description: 'Descripción',
      credit: 'Crédito',
      debit: 'Débito'
    },
    login: {
      title: 'Iniciar sesión',
      email: 'Email',
      password: 'Contraseña',
      submit: 'Ingresar'
    },
    user: {
      singular: 'Usuario',
      search: 'Buscar usuarios'
    },
    account: {
      singular: 'Cuenta',
      plural: 'Cuentas',
      create: 'Crear Cuenta',
      delete: '¿Eliminar Cuenta?',
      leaveTitle: '¿Dejar de ver Cuenta?',
      leaveAction: 'Dejar de ver',
      search: 'Buscar Cuentas',
      balance: 'Balance',
      detail: 'Detalle',
      sharing: 'Permisos',
      resume: 'Información de Cuenta',
      isOwner: 'Dueño',
      canEdit: 'Acceso de lectura y escritura',
      canNotEdit: 'Acceso de lectura solamente'
    },
    movement: {
      singular: 'Movimiento',
      plural: 'Movimientos',
      resume: 'Información de Movimiento',
      creator: 'Creado por',
      editor: 'Editado por',
      search: 'Buscar Movimientos',
      debit: 'Débito',
      credit: 'Crédito',
      noDesc: 'Sin descripción',
      description: 'Descripción',
      txType: 'Tipo de Movimiento',
      newBalance: 'Balance nuevo',
      currentBalance: 'Balance actual',
      delete: '¿Eliminar Movimiento?'
    },
    permission: {
      singular: 'Permiso',
      plural: 'Permisos',
      delete: '¿Revocar Acceso?',
      level: 'Nivel de Accesso',
      read: 'Lectura solamente',
      write: 'Lectura y escritura'
    },
    grouping: {
      singular: 'Grupo',
      plural: 'Grupos',
      create: 'Crear',
      search: 'Buscar Grupos',
      delete: '¿Eliminar Grupo?',
      detail: 'Detalle',
      total: 'Total',
      totals: 'Totales',
      noBalance: 'Sin información para mostrar balance',
      totalsByCcy: 'Totales por Moneda'
    },
    currency: {
      singular: 'Moneda',
      plural: 'Monedas',
      search: 'Buscar Monedas'
    },
    setting: {
      dateFormat: 'Formato de fechas',
      title: 'Ajuste',
      plural: 'Ajustes',
      defaultLanguage: 'Idioma por defecto a usar',
      updatePassword: 'Cambiar Contraseña',
      oldPassword: 'Contraseña actual',
      newPassword: 'Contraseña nueva',
      confirmPassword: 'Confirmar Contraseña nueva',
      mismatchPasswords: 'Confirmación y nueva Contraseña no coinciden',
      advance: 'Avanzado',
      currentVersion: 'Versión actual',
      currentServerVersion: 'Versión actual de servidor',
      update: 'Actualizar',
      updating: 'Actualizando',
      refreshTitle: '¿Buscar actualizaciones?',
      refreshSubtitle: 'Esta acción puede demorar'
    },
    notFound: {
      title: 'Página no encontrada',
      message: 'La página a la cual se está intentando acceder no existe',
      nav: 'Volver a inicio'
    },
    toast: {
      BR_AUTH_CLIENT: 'Error de autenticación',
      BR_MISSING_PARAM: 'No se envió un valor requerido',
      BR_PARAM_TOO_LONG: 'Valor muy largo',
      BR_INVALID_ID_VALUE: 'Valor de ID inválido',
      BR_EDIT_DELETED: 'No se puede editar un elemento previamente eliminado',
      BR_DELETE_DELETED: 'No se puede eliminar un elemento previamente eliminado',
      BR_USING_DELETED: 'No se puede usar un elemento previamente eliminado',
      BR_EXISTING_EMAIL: 'Ya existe un usuario con el email ingresado',
      BR_SUSPENDED_ACCOUNT: 'La cuenta se encuentra suspendida',
      BR_INVALID_SORT_PARAM: 'No se puede ordenar por el valor requerido',
      BR_GRANT_PERMISSION_TO_OWNER: 'No se puede otorgar permiso al dueño',
      BR_MAX_GROUPING_ELEMENTS: 'No se pueden agregar más Cuentas al Grupo',
      BR_MAX_GROUPING_CURRENCIES: 'El Grupo no puede tener más Cuentas con diferente Moneda',
      FORBIDDEN: 'Permisos insuficientes para acceder al elemento',
      NF_BY_ID: 'Elemento no encontrado por ID',
      NF_BY_EMAIL: 'Elemento no encontrado por Email',
      U_AUTH_CLIENT: 'Error de autenticación',
      U_ACCESS_DENIED: 'Necesita haber iniciado sesión',
      U_INVALID_CREDENTIALS: 'Email o Contraseña inválidas',
      U_SUSPENDED_USER: 'La cuenta se encuentra suspendida',
      U_INSUFFICIENT_PERMISSIONS: 'Permisos insuficientes',
      UNMAPPED: 'Algo inesperado ocurrió',
      password: 'Contraseña actualizada'
    }
  };

  public static register(translateService: TranslateService) {
    translateService.setTranslation('es', this.language);
  }

}
