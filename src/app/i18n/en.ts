import {LanguageSchema} from './language-schema';
import {TranslateService} from '@ngx-translate/core';

export class En {

  private static readonly language: LanguageSchema = {
    general: {
      name: 'Name',
      close: 'Close',
      cancel: 'Cancel',
      save: 'Save',
      new: 'New',
      add: 'Add',
      edit: 'Edit',
      delete: 'Delete',
      remove: 'Remove',
      select: 'Select',
      registered: 'Registered',
      loading: 'Loading',
      amount: 'Amount',
      view: 'View'
    },
    sort: {
      title: 'Sort by',
      none: 'None',
      creation: 'Creation',
      name: 'Name',
      registered: 'Registered',
      description: 'Description',
      credit: 'Credit',
      debit: 'Debit'
    },
    login: {
      title: 'Login',
      email: 'Email',
      password: 'Password',
      submit: 'Login'
    },
    user: {
      singular: 'User',
      search: 'Search Users'
    },
    account: {
      singular: 'Account',
      plural: 'Accounts',
      create: 'Create Account',
      delete: 'Delete account?',
      leaveTitle: 'Leave this account?',
      leaveAction: 'Leave',
      search: 'Search Accounts',
      balance: 'Balance',
      detail: 'Detail',
      sharing: 'Permissions',
      resume: 'Account information',
      isOwner: 'Owner',
      canEdit: 'Can view and edit movements',
      canNotEdit: 'Read only access'
    },
    movement: {
      singular: 'Movement',
      plural: 'Movements',
      resume: 'Movement information',
      creator: 'Created by',
      editor: 'Edited by',
      search: 'Search Movements',
      debit: 'Debit',
      credit: 'Credit',
      noDesc: 'No description',
      description: 'Description',
      txType: 'Movement type',
      newBalance: 'New balance',
      currentBalance: 'Current balance',
      delete: 'Delete Movement?'
    },
    permission: {
      singular: 'Permission',
      plural: 'Permissions',
      delete: 'Revoke access?',
      level: 'Access level',
      read: 'Read only',
      write: 'Read and write'
    },
    grouping: {
      singular: 'Grouping',
      plural: 'Groupings',
      create: 'Create',
      search: 'Search Groupings',
      delete: 'Delete Grouping?',
      detail: 'Detail',
      total: 'Total',
      totals: 'Totals',
      noBalance: 'No information to display balance',
      totalsByCcy: 'Totals by Currency'
    },
    currency: {
      singular: 'Currency',
      plural: 'Currencies',
      search: 'Search Currencies'
    },
    setting: {
      dateFormat: 'Date formats',
      title: 'Settings',
      plural: 'Settings',
      defaultLanguage: 'Default Language to use',
      updatePassword: 'Update Password',
      oldPassword: 'Old Password',
      newPassword: 'New Password',
      confirmPassword: 'Confirm new Password',
      mismatchPasswords: 'New and confirm Passwords do not match',
      advance: 'Advance',
      currentVersion: 'Current app Version',
      currentServerVersion: 'Current server Version',
      update: 'Update',
      updating: 'Updating',
      refreshTitle: 'Check for updates?',
      refreshSubtitle: 'This action can take a while'
    },
    notFound: {
      title: 'Page not found',
      message: 'The page that you are trying to access does not exists',
      nav: 'Go back home'
    },
    toast: {
      BR_AUTH_CLIENT: 'Authentication error',
      BR_MISSING_PARAM: 'Some needed value was missing',
      BR_PARAM_TOO_LONG: 'Value too long',
      BR_INVALID_ID_VALUE: 'Invalid id value',
      BR_EDIT_DELETED: 'Can not edit delete element',
      BR_DELETE_DELETED: 'Can not delete already deleted element',
      BR_USING_DELETED: 'Can not use deleted element',
      BR_FROM_AFTER_TO: 'From date can not be after To date',
      BR_EXISTING_EMAIL: 'Already exists an user with the provided email',
      BR_SUSPENDED_ACCOUNT: 'Permission can not be granted to suspended user',
      BR_INVALID_SORT_PARAM: 'Elements can not be sorted by the provided selection',
      BR_GRANT_PERMISSION_TO_OWNER: 'Permission can not be granted to owner',
      BR_MAX_GROUPING_ELEMENTS: 'Can not add more Accounts to Grouping',
      BR_MAX_GROUPING_CURRENCIES: 'Grouping can not contain more Accounts with different Currencies',
      FORBIDDEN: 'Insufficient permissions to access the element',
      NF_BY_ID: 'Element not found by id',
      NF_BY_EMAIL: 'Element not found by email',
      INTERNAL_SERVER: 'Something unexpected happen',
      U_AUTH_CLIENT: 'Authentication error',
      U_ACCESS_DENIED: 'You need to be logged in',
      U_INVALID_CREDENTIALS: 'Incorrect email or password',
      U_SUSPENDED_USER: 'This account has been suspended',
      U_INSUFFICIENT_PERMISSIONS: 'Insufficient permissions',
      UNMAPPED: 'Something unexpected happen'
    }
  };

  public static register(translateService: TranslateService) {
    translateService.setTranslation('en', this.language);
  }

}
