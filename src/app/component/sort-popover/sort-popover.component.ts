import {Component, OnInit} from '@angular/core';
import {NavParams, PopoverController} from '@ionic/angular';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-sort-popover',
  templateUrl: './sort-popover.component.html',
  styleUrls: ['./sort-popover.component.scss']
})
export class SortPopoverComponent implements OnInit {

  public config: SortConfig;

  constructor(
    private navParams: NavParams,
    public popoverController: PopoverController
  ) {
  }

  ngOnInit() {
    this.config = this.navParams.get('config');
  }

  public updateSort(prop: string, asc: boolean) {
    this.config.publisher.next({property: prop, ascending: asc});
  }
}

export class SortConfig {
  sortedBy: string;
  ascending: boolean;
  elements: SortElement[];
  currentSort: Sort;
  publisher?: Subject<Sort>;
}

export class SortElement {
  title: string;
  property: string;
}

export class Sort {
  property: string;
  ascending: boolean;
}
