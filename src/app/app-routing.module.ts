import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {AuthService} from './services/auth.service';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthService],
    loadChildren: () => import('./home/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'login',
    canActivate: [AuthService],
    loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: '**',
    loadChildren: () => import('./not-found/not-found.module').then( m => m.NotFoundPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
