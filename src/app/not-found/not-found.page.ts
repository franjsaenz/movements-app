import {Component, OnInit} from '@angular/core';
import {LanguageService} from '../services/language.service';
import {Router} from '@angular/router';
import {SettingService} from '../services/setting.service';
import {User} from '../entity/user';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.page.html',
  styleUrls: ['./not-found.page.scss']
})
export class NotFoundPage implements OnInit {

  loaded = false;

  loggedIn = false;

  constructor(
    private settingService: SettingService,
    private languageService: LanguageService,
    private router: Router
  ) {
    settingService.getSetting(SettingService.userKey).then((user: User) => {
      this.loggedIn = user != null;
      this.loaded = true;
    });
  }

  ngOnInit() {
    this.languageService.setTitle('notFound.title').then();
  }

  navigate() {
    if (this.loggedIn) {
      this.router.navigate(['accounts'], {replaceUrl: true}).then();
    } else {
      this.router.navigate(['login'], {replaceUrl: true}).then();
    }
  }

}
