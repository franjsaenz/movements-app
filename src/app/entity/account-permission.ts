import {User} from './user';

export class AccountPermission {
  user?: User;
  permission?: string;
}
