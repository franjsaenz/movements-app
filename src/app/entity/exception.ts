export class Exception {
  timestamp: string;
  status: number;
  error: string;
  message: string;
  path: string;
  meta: { [K: string]: any };
  errorCode: string;
}
