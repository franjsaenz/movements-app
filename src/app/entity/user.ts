export class User {
  id?: any;
  created?: string;
  updated?: string;
  deleted?: string;
  name?: string;
  movementUser?: User;
}
