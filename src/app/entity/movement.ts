import {User} from './user';

export class Movement {
  id?: number;
  created?: string;
  creator?: User;
  updated?: string;
  updater?: User;
  deleted?: string;
  registered?: string;
  description?: string;
  credit?: number;
  debit?: number;
}
