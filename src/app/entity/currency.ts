export class Currency {
  id: number;
  isoCode: string;
  symbol: string;
  name: string;
}
