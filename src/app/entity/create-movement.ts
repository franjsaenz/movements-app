export class CreateMovement {
  registered: string;
  description?: string;
  amount: number;
  debit: boolean;
}
