import {Currency} from './currency';
import {User} from './user';

export class Account {
  id?: number;
  created?: string;
  updated?: string;
  deleted?: string;
  currency?: Currency;
  name: string;
  canEdit?: boolean;
  owner?: boolean;
  creator?: User;
  balance?: number;
}
