export class Grouping {
  id?: number;
  created?: string;
  updated?: string;
  deleted?: string;
  name: string;
  balances?: Array<Balance>;
}

export class Balance {
  currencyId?: number;
  currencyName?: string;
  currencySymbol?: string;
  byAccount?: { [k: string]: number };
  total?: number;
}
