import {User} from './user';

export class Credential {
  id?: string;
  created?: string;
  movementUser?: User;
  issuedAt?: string;
  expiration?: string;
  notBefore?: string;
  token?: string;
}
