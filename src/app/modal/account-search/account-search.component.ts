import {Component, OnInit, ViewChild} from '@angular/core';
import {Account} from '../../entity/account';
import {IonInfiniteScroll, IonSearchbar, ModalController} from '@ionic/angular';
import {AccountService} from '../../services/account.service';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {Page} from '../../entity/page';
import {HttpErrorResponse} from '@angular/common/http';
import {Currency} from '../../entity/currency';

@Component({
  selector: 'app-account-search',
  templateUrl: './account-search.component.html',
  styleUrls: ['./account-search.component.scss']
})
export class AccountSearchComponent implements OnInit {

  loadingContent = true;

  searchTerm: string;

  pageNumber = 1;

  accounts: Array<Account> = [];

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  @ViewChild(IonSearchbar) searchBar: IonSearchbar;

  constructor(
    private accountService: AccountService,
    private errorMappingService: ErrorMappingService,
    private modalController: ModalController
  ) {
  }

  ngOnInit() {
    this.listContent();
    setTimeout(() => {
      if (this.searchBar != null) {
        this.searchBar.setFocus().then();
      }
    }, 500);
  }

  listContent() {
    this.loadingContent = true;
    if (this.infiniteScroll != null) {
      this.infiniteScroll.disabled = true;
    }
    this.accountService.search(
      this.searchTerm, this.pageNumber - 1,
      'name',
      true
    ).subscribe((resp: Page<Currency>) => {
      this.loadingContent = false;
      this.accounts = this.accounts.concat(resp.content);
      this.pageNumber++;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = resp.last;
    }, (err: HttpErrorResponse) => {
      this.loadingContent = false;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = true;
      this.errorMappingService.displayError(err.error, ErrorMappingService.WARNING).then();
    });
  }

  search(term: string) {
    this.resetList();
    if (term == null || term.length < 1) {
      this.searchTerm = null;
    } else {
      this.searchTerm = term;
    }
    this.listContent();
  }

  resetList() {
    this.searchTerm = null;
    this.accounts = [];
    this.pageNumber = 1;
  }

  closeModal(account: Account = null) {
    this.modalController.dismiss(account).then();
  }

}
