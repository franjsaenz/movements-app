import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {User} from '../../entity/user';
import {IonInfiniteScroll, IonSearchbar, ModalController} from '@ionic/angular';
import {UserService} from '../../services/user.service';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {Page} from '../../entity/page';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-user-search',
  templateUrl: './user-search.component.html',
  styleUrls: ['./user-search.component.scss']
})
export class UserSearchComponent implements OnInit {

  @Input() ignoreUser: User;

  loadingContent = true;

  searchTerm: string;

  pageNumber = 1;

  users: Array<User> = [];

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  @ViewChild(IonSearchbar) searchBar: IonSearchbar;

  constructor(
    private userService: UserService,
    private errorMappingService: ErrorMappingService,
    private modalController: ModalController
  ) {
  }

  ngOnInit() {
    this.listContent();
    setTimeout(() => {
      if (this.searchBar != null) {
        this.searchBar.setFocus().then();
      }
    }, 500);
  }

  listContent() {
    if (this.searchTerm == null || this.searchTerm.length < 1) {
      this.loadingContent = false;
      if (this.infiniteScroll != null) {
        this.infiniteScroll.complete();
        this.infiniteScroll.disabled = true;
      }
      return;
    }
    this.loadingContent = true;
    if (this.infiniteScroll != null) {
      this.infiniteScroll.disabled = true;
    }
    this.userService.list(
      this.searchTerm, this.pageNumber - 1,
      'name', true
    ).subscribe((resp: Page<User>) => {
      this.loadingContent = false;
      this.users = this.users.concat(resp.content);
      this.pageNumber++;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = resp.last;
    }, (err: HttpErrorResponse) => {
      this.loadingContent = false;
      this.errorMappingService.displayError(err.error, ErrorMappingService.WARNING).then();
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = true;
    });
  }

  search(term: string) {
    this.resetList();
    if (term == null || term.length < 1) {
      this.searchTerm = null;
    } else {
      this.searchTerm = term;
    }
    this.listContent();
  }

  resetList() {
    this.searchTerm = null;
    this.users = [];
    this.pageNumber = 1;
  }

  closeModal(user: User = null) {
    this.modalController.dismiss(user).then();
  }

}
