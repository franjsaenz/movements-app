import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MovementService} from '../../services/movement.service';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {ActionSheetService} from '../../services/action-sheet.service';
import {CreateMovement} from '../../entity/create-movement';
import {ModalController} from '@ionic/angular';
import {HttpErrorResponse} from '@angular/common/http';
import {Response} from '../../entity/response';
import {Account} from '../../entity/account';
import {Movement} from '../../entity/movement';
import {DateFormat, SettingService} from '../../services/setting.service';

@Component({
  selector: 'app-movement-detail',
  templateUrl: './movement-detail.component.html',
  styleUrls: ['./movement-detail.component.scss']
})
export class MovementDetailComponent implements OnInit {

  dateFormat: DateFormat;

  form: FormGroup;

  @Input() account: Account;

  @Input() movement: Movement;

  editing: boolean;

  isSending = false;

  newBalance: number;

  constructor(
    private movementService: MovementService,
    private settingService: SettingService,
    private errorMappingService: ErrorMappingService,
    private actionSheetService: ActionSheetService,
    private modalController: ModalController,
    private formBuilder: FormBuilder
  ) {
    settingService.getDefaultDateFormat().then((dateFormat: DateFormat) => {
      this.dateFormat = dateFormat;
    });
  }

  ngOnInit() {
    this.calculateNewBalance(0);
    this.editing = this.movement == null;
    this.initForm();
    if (this.editing) {
      this.edit();
    } else {
      this.disable();
    }
  }

  private initForm() {
    this.form = this.formBuilder.group({
      registered: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.maxLength(100)]),
      amount: new FormControl('', [Validators.required]),
      debit: new FormControl('', [Validators.required])
    });
    this.form.get('amount').valueChanges.subscribe((value: number) => {
      this.calculateNewBalance(value);
    });
    this.form.get('debit').valueChanges.subscribe(() => {
      this.calculateNewBalance(this.form.get('amount').value);
    });
    this.resetForm();
  }

  private resetForm() {
    const registered: string = this.movement == null || this.movement.registered == null ?
      new Date().toISOString() : this.movement.registered;
    const description: string = this.movement == null || this.movement.description == null ?
      '' : this.movement.description;
    const amount: number = this.movement == null || (this.movement.credit == null && this.movement.debit == null) ?
      0 : (this.movement.debit == null ? this.movement.credit : this.movement.debit);
    const debit: boolean = this.movement == null || this.movement.debit != null;
    this.form.get('registered').reset(registered);
    this.form.get('description').reset(description);
    this.form.get('amount').reset(amount);
    this.form.get('debit').reset(debit ? 'debit' : 'credit');
  }

  private disable() {
    this.form.get('registered').disable();
    this.form.get('description').disable();
    this.form.get('amount').disable();
    this.form.get('debit').disable();
  }

  private enable() {
    this.form.get('registered').enable();
    this.form.get('description').enable();
    this.form.get('amount').enable();
    this.form.get('debit').enable();
  }

  edit() {
    this.editing = true;
    this.enable();
    this.form.markAsTouched();
  }

  reset() {
    if (this.movement == null) {
      this.close();
      return;
    }
    this.resetForm();
    this.disable();
    this.editing = false;
  }

  save() {
    const toSend: CreateMovement = {
      registered: this.form.get('registered').value,
      description: this.form.get('description').value,
      amount: this.form.get('amount').value,
      debit: this.form.get('debit').value === 'debit'
    };
    const registered: Date = new Date(toSend.registered);
    registered.setMilliseconds(0);
    toSend.registered = registered.toISOString();
    this.isSending = true;
    if (this.movement != null) {
      this.movementService.update(this.movement, toSend)
      .subscribe((movement: Movement) => {
        this.onSuccess(movement);
      }, (error: HttpErrorResponse) => {
        this.onError(error);
      });
    } else {
      this.movementService.create(this.account, toSend)
      .subscribe((movement: Movement) => {
        this.onSuccess(movement);
      }, (error: HttpErrorResponse) => {
        this.onError(error);
      });
    }
  }

  private onSuccess(movement: Movement) {
    this.movement = movement;
    this.account.balance = this.newBalance;
    this.calculateNewBalance(movement.debit != null ? movement.debit : movement.credit);
    this.isSending = false;
    this.reset();
  }

  private onError(error: HttpErrorResponse) {
    this.isSending = false;
    this.editing = true;
    this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING).then();
  }

  async delete() {
    const handler = () => {
      this.isSending = true;
      this.movementService.delete(this.movement).subscribe(
        (ignored: Response) => {
          this.movement = null;
          this.account.balance = this.newBalance;
          this.close();
        }, (error: HttpErrorResponse) => {
          this.onError(error);
          this.editing = false;
        }
      );
    };
    await this.actionSheetService.presentDelete('movement.delete', this.movement.description,
      'general.delete', 'general.cancel', handler
    );
  }

  close() {
    this.modalController.dismiss(this.movement).then();
  }

  private calculateNewBalance(value: number) {
    if (value == null || isNaN(value)) {
      value = 0;
    }
    const debit: boolean = this.form != null ? this.form.get('debit').value === 'debit' : true;
    let balance: number = this.account.balance;
    if (this.movement != null) {
      balance = balance -
        (this.movement.credit != null ? this.movement.credit : 0) +
        (this.movement.debit != null ? this.movement.debit : 0);
    }
    this.newBalance = debit ? balance - value : balance + value;
  }

}
