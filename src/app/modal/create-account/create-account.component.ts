import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AccountService} from '../../services/account.service';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {ModalService} from '../../services/modal.service';
import {ModalController} from '@ionic/angular';
import {Account} from '../../entity/account';
import {Currency} from '../../entity/currency';
import {HttpErrorResponse} from '@angular/common/http';
import {CurrencySearchComponent} from '../currency-search/currency-search.component';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss']
})
export class CreateAccountComponent implements OnInit {

  form: FormGroup;

  selectedCurrency: Currency;

  isSending = false;

  constructor(
    private accountService: AccountService,
    private errorMappingService: ErrorMappingService,
    private modalService: ModalService,
    private modalController: ModalController,
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.form = this.formBuilder.group({
      name: new FormControl('', [Validators.required, Validators.maxLength(100)]),
      currency: new FormControl('', [Validators.required])
    });
  }

  save() {
    const toSend: Account = {
      name: this.form.get('name').value
    };
    this.isSending = true;
    this.accountService.create(toSend, this.selectedCurrency).subscribe(
      (account: Account) => {
        this.close(account);
      }, (error: HttpErrorResponse) => {
        this.isSending = false;
        this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING).then();
      }
    );
  }

  async searchCurrency() {
    const response = await this.modalService.presentModal(CurrencySearchComponent);
    if (response != null) {
      this.selectedCurrency = response;
      this.form.get('currency').setValue(this.selectedCurrency.name);
    }
  }

  close(account: Account = null) {
    this.modalController.dismiss(account).then();
  }

}
