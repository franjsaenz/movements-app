import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {GroupingService} from '../../services/grouping.service';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {ModalController} from '@ionic/angular';
import {HttpErrorResponse} from '@angular/common/http';
import {Grouping} from '../../entity/grouping';

@Component({
  selector: 'app-create-grouping',
  templateUrl: './create-grouping.component.html',
  styleUrls: ['./create-grouping.component.scss']
})
export class CreateGroupingComponent implements OnInit {

  form: FormGroup;

  isSending = false;

  constructor(
    private groupingService: GroupingService,
    private errorMappingService: ErrorMappingService,
    private modalController: ModalController,
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.form = this.formBuilder.group({
      name: new FormControl('', [Validators.required, Validators.maxLength(100)])
    });
  }

  save() {
    const toSend: Grouping = {
      name: this.form.get('name').value
    };
    this.isSending = true;
    this.groupingService.create(toSend).subscribe(
      (grouping: Grouping) => {
        this.close(grouping);
      }, (error: HttpErrorResponse) => {
        this.isSending = false;
        this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING).then();
      }
    );
  }

  close(grouping: Grouping = null) {
    this.modalController.dismiss(grouping).then();
  }

}
