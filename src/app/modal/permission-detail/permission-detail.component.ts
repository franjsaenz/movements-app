import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Account} from '../../entity/account';
import {PermissionService} from '../../services/permission.service';
import {DateFormat, SettingService} from '../../services/setting.service';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {ActionSheetService} from '../../services/action-sheet.service';
import {ModalController} from '@ionic/angular';
import {AccountPermission} from '../../entity/account-permission';
import {User} from '../../entity/user';
import {HttpErrorResponse} from '@angular/common/http';
import {Response} from '../../entity/response';
import {ModalService} from '../../services/modal.service';
import {UserSearchComponent} from '../user-search/user-search.component';

@Component({
  selector: 'app-permission-detail',
  templateUrl: './permission-detail.component.html',
  styleUrls: ['./permission-detail.component.scss']
})
export class PermissionDetailComponent implements OnInit {

  private user: User;

  dateFormat: DateFormat;

  form: FormGroup;

  @Input() account: Account;

  @Input() permission: AccountPermission;

  selectedUser: User;

  editing: boolean;

  isSending = false;

  constructor(
    private permissionService: PermissionService,
    private settingService: SettingService,
    private errorMappingService: ErrorMappingService,
    private actionSheetService: ActionSheetService,
    private modalService: ModalService,
    private modalController: ModalController,
    private formBuilder: FormBuilder
  ) {
    settingService.getDefaultDateFormat().then((dateFormat: DateFormat) => {
      this.dateFormat = dateFormat;
    });
    settingService.getSetting(SettingService.userKey).then((user: User) => {
      this.user = user;
    });
  }

  ngOnInit() {
    this.editing = this.permission == null;
    if (this.permission != null) {
      this.selectedUser = this.permission.user;
    }
    this.initForm();
    if (this.editing) {
      this.edit();
    } else {
      this.disable();
    }
  }

  private initForm() {
    this.form = this.formBuilder.group({
      user: new FormControl('', [Validators.required]),
      permission: new FormControl('', [Validators.required])
    });
    this.resetForm();
  }

  private resetForm() {
    const user: string = this.selectedUser == null ? '' : this.selectedUser.name;
    const permission: string = this.permission == null ? 'READ' : this.permission.permission;
    this.form.get('user').reset(user);
    this.form.get('permission').reset(permission);
  }

  private disable() {
    this.form.get('user').disable();
    this.form.get('permission').disable();
  }

  private enable() {
    this.form.get('user').enable();
    this.form.get('permission').enable();
  }

  edit() {
    this.editing = true;
    this.enable();
    this.form.markAsTouched();
  }

  reset() {
    if (this.permission == null) {
      this.close();
      return;
    }
    this.selectedUser = this.permission.user;
    this.resetForm();
    this.disable();
    this.editing = false;
  }

  save() {
    this.isSending = true;
    this.permissionService.grant(this.account, this.selectedUser, this.form.get('permission').value)
    .subscribe((permission: AccountPermission) => {
      this.onSuccess(permission);
    }, (error: HttpErrorResponse) => {
      this.onError(error);
    });
  }

  private onSuccess(permission: AccountPermission) {
    this.permission = permission;
    this.selectedUser = permission.user;
    this.isSending = false;
    this.reset();
  }

  private onError(error: HttpErrorResponse) {
    this.isSending = false;
    this.editing = true;
    this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING).then();
  }

  async delete() {
    const handler = () => {
      this.isSending = true;
      this.permissionService.revoke(this.account, this.selectedUser).subscribe(
        (ignored: Response) => {
          this.permission = null;
          this.close();
        }, (error: HttpErrorResponse) => {
          this.onError(error);
          this.editing = false;
        }
      );
    };
    await this.actionSheetService.presentDelete('permission.delete', null,
      'general.delete', 'general.cancel', handler
    );
  }

  async searchUser() {
    const user: User = await this.modalService.presentModal(UserSearchComponent, {ignoreUser: this.user});
    if (user != null) {
      this.selectedUser = user;
      this.form.get('user').reset(this.selectedUser.name);
    }
  }

  close() {
    this.modalController.dismiss(this.permission).then();
  }

}
