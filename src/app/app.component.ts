import {Component} from '@angular/core';

import {Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {TranslateService} from '@ngx-translate/core';
import {Language, LanguageService} from './services/language.service';
import {En} from './i18n/en';
import {Es} from './i18n/es';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private languageService: LanguageService,
    private translateService: TranslateService,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp().then();
  }

  async initializeApp() {
    await this.platform.ready();
    if (this.platform.is('cordova') || this.platform.is('pwa')) {
      this.statusBar.styleDefault();
    }
    await this.initTranslate();
    if (this.platform.is('cordova') || this.platform.is('pwa')) {
      this.splashScreen.hide();
    }
  }

  private async initTranslate() {
    const def: string = this.translateService.getBrowserLang();
    const using: Language = await this.languageService.getStored();
    En.register(this.translateService);
    Es.register(this.translateService);
    await this.translateService.use(using != null ? using.code : (def == null || def.length < 1 ? 'en' : def)).toPromise();
  }

}
