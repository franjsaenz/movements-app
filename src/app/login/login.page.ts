import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {AuthService} from '../services/auth.service';
import {ErrorMappingService} from '../services/error-mapping.service';
import {Language, LanguageService} from '../services/language.service';
import {Login} from '../entity/login';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;

  isSending: boolean;

  languages: Array<Language>;

  defaultLang: Language;

  constructor(
    private authService: AuthService,
    private errorMappingService: ErrorMappingService,
    private languageService: LanguageService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    languageService.setTitle('login.title');
    this.languages = languageService.getLanguages();
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('', [
        Validators.required
      ])
    });
    this.languageService.getStored().then((defLang: Language) => {
      this.defaultLang = defLang;
    });
  }

  updateDefaultLang(lang: Language) {
    this.defaultLang = null;
    this.languageService.useLanguage(lang).then(() => {
      setTimeout(() => {
        this.defaultLang = lang;
      }, 500);
    });
  }

  public submit() {
    if (this.isSending) {
      return;
    }
    const login: Login = {
      platformIdentifier: this.loginForm.get('email').value,
      password: this.loginForm.get('password').value
    };
    this.isSending = true;
    this.authService.login(login).subscribe(
      (cred: Credential) => {
        this.authService.storeLoggedIn(cred, true)
        .then(() => {
          this.isSending = false;
          this.loginForm.reset();
          this.router.navigate([''], {replaceUrl: true}).then();
        })
        .catch((err) => {
          console.error(err);
          this.isSending = false;
          this.errorMappingService.displayError(null, ErrorMappingService.WARNING).then();
        });
      },
      (error: HttpErrorResponse) => {
        this.isSending = false;
        this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING).then();
      }
    );
  }

}
