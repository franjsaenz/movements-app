import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Sortable} from './sortable';
import {QueryBuilder} from './QueryBuilder';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Grouping} from '../entity/grouping';
import {Page} from '../entity/page';
import {Response} from '../entity/response';
import {Account} from '../entity/account';

@Injectable({
  providedIn: 'root'
})
export class GroupingService {

  private baseUrl: string = environment.baseUrl + '/groupings';

  private sortable: Sortable;

  private elementSortable: Sortable;

  constructor(
    private queryBuilder: QueryBuilder,
    private http: HttpClient
  ) {
    this.sortable = new Sortable({
      sortedBy: null,
      ascending: null,
      elements: [{
        title: 'sort.creation',
        property: 'created'
      }, {
        title: 'sort.name',
        property: 'name'
      }],
      currentSort: {
        property: 'name',
        ascending: true
      }
    });
    this.elementSortable = new Sortable({
      sortedBy: null,
      ascending: null,
      elements: [{
        title: 'sort.creation',
        property: 'created'
      }, {
        title: 'sort.name',
        property: 'account.name'
      }],
      currentSort: {
        property: 'account.name',
        ascending: true
      }
    });
  }

  public getSortable(): Sortable {
    return this.sortable;
  }

  public getElementSortable(): Sortable {
    return this.elementSortable;
  }

  public search(term: string, pageNumber: number = 0, sortBy: string = null, ascending: boolean = null): Observable<Page<Grouping>> {
    const params: { [k: string]: any } = this.queryBuilder.standard(term, pageNumber, sortBy, ascending);
    return this.http.get<Page<Grouping>>(this.baseUrl, {params});
  }

  public create(toCreate: Grouping): Observable<Grouping> {
    return this.http.post<Grouping>(this.baseUrl, toCreate);
  }

  public get(groupingId: number): Observable<Grouping> {
    return this.http.get<Grouping>(this.baseUrl + '/' + groupingId);
  }

  public update(grouping: Grouping, update: Grouping): Observable<Grouping> {
    return this.http.put<Grouping>(this.baseUrl + '/' + grouping.id, update);
  }

  public delete(grouping: Grouping): Observable<Response> {
    return this.http.delete<Response>(this.baseUrl + '/' + grouping.id);
  }

  public listAccounts(
    grouping: Grouping, pageNumber: number = 0,
    sortBy: string = null, ascending: boolean = null
  ): Observable<Page<Account>> {
    const params: { [k: string]: any } = this.queryBuilder.withoutTerms(pageNumber, sortBy, ascending);
    return this.http.get<Page<Account>>(this.baseUrl + '/' + grouping.id + '/accounts', {params});
  }

  public addAccount(grouping: Grouping, account: Account): Observable<Account> {
    const params: { [k: string]: any } = {accountId: account.id};
    return this.http.post<Account>(this.baseUrl + '/' + grouping.id + '/accounts', null, {params});
  }

  public removeAccount(grouping: Grouping, account: Account): Observable<Response> {
    const params: { [k: string]: any } = {accountId: account.id};
    return this.http.delete<Response>(this.baseUrl + '/' + grouping.id + '/accounts', {params});
  }

}
