import {Injectable} from '@angular/core';
import {Exception} from '../entity/exception';
import {TranslateService} from '@ngx-translate/core';
import {ToastController} from '@ionic/angular';
import {ToastOptions} from '@ionic/core';

@Injectable({
  providedIn: 'root'
})
export class ErrorMappingService {

  public static WARNING = 'warning';
  public static SUCCESS = 'success';

  private presentedToast: HTMLIonToastElement;

  constructor(
    private toastController: ToastController,
    private translateService: TranslateService
  ) {
  }

  public async displayError(error: Exception, toastColor: string, toastDuration: number = null) {
    let toEvaluate: string = null;
    if (error != null) {
      toEvaluate = error.meta != null && error.meta.originalErrorCode != null ? error.meta.originalErrorCode : error.errorCode;
    }
    const key = toEvaluate != null != null ? 'toast.' + toEvaluate : 'toast.UNMAPPED';
    await this.displayToast(key, toastColor, toastDuration);
  }

  public async displayToast(msgCode: string, toastColor: string, toastDuration: number = null) {
    const msg = await this.getErrorMessage(msgCode);
    const close = await this.translateService.get('general.close').toPromise();
    const config: ToastOptions = {
      message: msg,
      position: 'bottom',
      animated: true,
      color: toastColor,
      duration: toastDuration == null ? 0 : toastDuration,
      buttons: [
        {
          text: close,
          role: 'cancel'
        }
      ]
    };
    if (this.presentedToast != null) {
      await this.presentedToast.dismiss();
    }
    this.presentedToast = await this.toastController.create(config);
    this.presentedToast.onWillDismiss().then(() => {
      this.presentedToast = null;
    });
    this.presentedToast.present().then();
  }

  private async getErrorMessage(key: string): Promise<string> {
    const value = await this.translateService.get(key).toPromise();
    if (value === key) {
      return await this.translateService.get('toast.UNMAPPED').toPromise();
    } else {
      return value;
    }
  }

}
