import {Sort, SortConfig} from '../component/sort-popover/sort-popover.component';
import {Subject, Subscription} from 'rxjs';

export class Sortable {

  private sortingObservable = new Subject<Sort>();

  public constructor(
    private sortConfig: SortConfig
  ) {
    if (sortConfig != null) {
      sortConfig.publisher = this.sortingObservable;
    }
  }

  public getSortConfig(): SortConfig {
    return this.sortConfig;
  }

  public subscribeToSort(sub: (sort: Sort) => void): Subscription {
    return this.sortingObservable.asObservable().subscribe(sub);
  }

}
