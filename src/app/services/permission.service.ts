import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {QueryBuilder} from './QueryBuilder';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Page} from '../entity/page';
import {AccountPermission} from '../entity/account-permission';
import {Account} from '../entity/account';
import {User} from '../entity/user';
import {Response} from '../entity/response';

@Injectable({
  providedIn: 'root'
})
export class PermissionService {

  private baseUrl: string = environment.baseUrl + '/accounts/';
  private permissionUrl = '/permissions';

  constructor(
    private queryBuilder: QueryBuilder,
    private http: HttpClient
  ) {
  }

  public list(account: Account, pageNumber: number, sortBy: string, ascending: boolean): Observable<Page<AccountPermission>> {
    const params: { [k: string]: any } = this.queryBuilder.withoutTerms(pageNumber, sortBy, ascending);
    return this.http.get<Page<AccountPermission>>(this.baseUrl + account.id + this.permissionUrl, {params});
  }

  public grant(account: Account, user: User, permission: string): Observable<AccountPermission> {
    const params: { [k: string]: any } = {permission, userId: user.id};
    return this.http.post<AccountPermission>(this.baseUrl + account.id + this.permissionUrl, {}, {params});
  }

  public revoke(account: Account, user: User): Observable<Response> {
    const params: { [k: string]: any } = {userId: user.id};
    return this.http.delete<Response>(this.baseUrl + account.id + this.permissionUrl, {params});
  }

}
