import {Injectable} from '@angular/core';
import {Storage} from '@capacitor/core';

@Injectable({
  providedIn: 'root'
})
export class SettingService {

  public static userKey = 'LOGGED_USER';

  public static dateFormatKey = 'DATE_FORMAT';

  public static overrideLanguage = 'OVERRIDE_LANGUAGE';

  private dateFormats: { [key: string]: DateFormat; } = {
    DEFAULT_24: {
      pipe: 'yyyy/MM/dd HH:mm:ss',
      selector: 'YYYY/MM/DD HH:mm:ss'
    },
    DEFAULT_12: {
      pipe: 'yyyy/MM/dd hh:mm:ss a',
      selector: 'YYYY/MM/DD hh:mm:ss a'
    },
    TEXT_MONTH_DAY_24: {
      pipe: 'MMM d, y, HH:mm:ss',
      selector: 'MMM D, YYYY, HH:mm:ss'
    },
    TEXT_MONTH_DAY_12: {
      pipe: 'MMM d, y, hh:mm:ss a',
      selector: 'MMM D, YYYY, hh:mm:ss a'
    },
    DAY_TEXT_MONTH_24: {
      pipe: 'd MMM, y, HH:mm:ss',
      selector: 'D MMM, YYYY, HH:mm:ss'
    },
    DAY_TEXT_MONTH_12: {
      pipe: 'd MMM, y, hh:mm:ss a',
      selector: 'D MMM, YYYY, hh:mm:ss a'
    },
    DAY_MONTH_24: {
      pipe: 'dd-MM-yyyy HH:mm:ss',
      selector: 'DD-MM-YYYY HH:mm:ss'
    },
    DAY_MONTH_12: {
      pipe: 'dd-MM-yyyy hh:mm:ss a',
      selector: 'DD-MM-YYYY hh:mm:ss a'
    }
  };

  constructor() {
  }

  public getDateFormats(): DateFormat[] {
    const df: DateFormat[] = [];
    Object.entries(this.dateFormats).forEach((val: [string, DateFormat]) => {
      const toPush: DateFormat = {
        id: val[0],
        pipe: val[1].pipe,
        selector: val[1].selector
      };
      df.push(toPush);
    });
    return df;
  }

  public getDateFormat(selectedId?: string): DateFormat {
    if (selectedId == null || selectedId.length < 1) {
      selectedId = 'DEFAULT_24';
    }
    const selected: DateFormat = this.dateFormats[selectedId];
    if (selected == null) {
      return this.getDateFormat(null);
    }
    return {
      id: selectedId,
      pipe: selected.pipe,
      selector: selected.selector
    };
  }

  public async getDefaultDateFormat(): Promise<DateFormat> {
    const dateFormat: DateFormat = await this.getSetting(SettingService.dateFormatKey);
    if (dateFormat == null) {
      return this.getDateFormat();
    } else {
      return dateFormat;
    }
  }

  public async storeSetting(settingKey: string, setting: any) {
    await Storage.set({
      key: settingKey,
      value: JSON.stringify(setting)
    });
  }

  public async getSetting(settingKey: string): Promise<any> {
    const value = await Storage.get({key: settingKey});
    return JSON.parse(value.value);
  }

  public async clearStorage() {
    await Storage.clear();
  }

}

export class DateFormat {
  id?: string;
  pipe: string;
  selector: string;
}
