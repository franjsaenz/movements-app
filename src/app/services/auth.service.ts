import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {SettingService} from './setting.service';
import {from, Observable} from 'rxjs';
import {Status} from '../entity/status';
import {Login} from '../entity/login';
import {switchMap} from 'rxjs/operators';
import {Credential} from '../entity/credential';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements HttpInterceptor, CanActivate {

  private baseUrl: string = environment.baseUrl;

  constructor(
    private settingService: SettingService,
    private http: HttpClient,
    private router: Router
  ) {
  }

  public status(): Observable<Status> {
    return this.http.get<Status>(this.baseUrl);
  }

  public login(login: Login): Observable<Credential> {
    return this.http.post<Credential>(this.baseUrl + '/login', login);
  }

  public async storeLoggedIn(cred: Credential, force: boolean = false) {
    const stored: Credential = await this.getLoggedIn();
    if (force || stored == null || stored.movementUser == null || stored.movementUser.id !== cred.movementUser.id) {
      await this.settingService.storeSetting(SettingService.userKey, cred);
    }
  }

  public async getLoggedIn(): Promise<Credential> {
      return await this.settingService.getSetting(SettingService.userKey);
  }

  public async logout() {
    await this.settingService.clearStorage();
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const promisedUser: Observable<Credential> = from(this.getLoggedIn());
    return promisedUser.pipe(switchMap((cred: Credential) => {
      let h = req.headers;
      h = h.set('Content-Type', 'application/json');
      if (cred != null && cred.token != null) {
        let bearer = cred.token;
        if (!bearer.includes('Bearer ')) {
          bearer = 'Bearer ' + bearer;
        }
        h = h.set('Authorization', bearer);
      }
      const clonedRequest = req.clone({headers: h});
      return next.handle(clonedRequest);
    }));
  }

  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    const cred: Credential = await this.getLoggedIn();
    const loggedIn = cred != null && cred.token != null;
    if (state.url.includes('login')) {
      if (loggedIn) {
        this.router.navigate([''], {replaceUrl: true}).then();
        return false;
      }
    } else {
      if (!loggedIn) {
        this.router.navigate(['login'], {replaceUrl: true}).then();
        return false;
      }
    }
    return true;
  }

}
