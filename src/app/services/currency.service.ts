import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Page} from '../entity/page';
import {Currency} from '../entity/currency';
import {environment} from '../../environments/environment';
import {QueryBuilder} from './QueryBuilder';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {

  private baseUrl: string = environment.baseUrl + '/currencies';

  constructor(
    private queryBuilder: QueryBuilder,
    private http: HttpClient
  ) {
  }

  public search(term: string, pageNumber: number = 0, sortBy: string = null, ascending: boolean = null): Observable<Page<Currency>> {
    const params: { [k: string]: any } = this.queryBuilder.standard(term, pageNumber, sortBy, ascending);
    return this.http.get<Page<Currency>>(this.baseUrl, {params});
  }

}
