import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Sortable} from './sortable';
import {Observable} from 'rxjs';
import {Page} from '../entity/page';
import {Account} from '../entity/account';
import {Currency} from '../entity/currency';
import {Response} from '../entity/response';
import {QueryBuilder} from './QueryBuilder';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private baseUrl: string = environment.baseUrl + '/accounts';

  private sortable: Sortable;

  constructor(
    private queryBuilder: QueryBuilder,
    private http: HttpClient
  ) {
    this.sortable = new Sortable({
      sortedBy: null,
      ascending: null,
      elements: [{
        title: 'sort.creation',
        property: 'created'
      }, {
        title: 'sort.name',
        property: 'name'
      }],
      currentSort: {
        property: 'name',
        ascending: true
      }
    });
  }

  public getSortable(): Sortable {
    return this.sortable;
  }

  public search(term: string, pageNumber: number = 0, sortBy: string = null, ascending: boolean = null): Observable<Page<Account>> {
    const params: { [k: string]: any } = this.queryBuilder.standard(term, pageNumber, sortBy, ascending);
    return this.http.get<Page<Account>>(this.baseUrl, {params});
  }

  public create(account: Account, currency: Currency): Observable<Account> {
    const params: { [k: string]: any } = {currencyId: currency.id};
    return this.http.post<Account>(this.baseUrl, account, {params});
  }

  public get(accountId: number): Observable<Account> {
    return this.http.get<Account>(this.baseUrl + '/' + accountId);
  }

  public update(account: Account, update: Account, currency: Currency): Observable<Account> {
    const params: { [k: string]: any } = {currencyId: currency.id};
    return this.http.put<Account>(this.baseUrl + '/' + account.id, update, {params});
  }

  public delete(account: Account): Observable<Response> {
    return this.http.delete<Response>(this.baseUrl + '/' + account.id);
  }

}
