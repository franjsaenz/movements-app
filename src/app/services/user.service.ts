import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ChangePassword} from '../entity/change-password';
import {Observable} from 'rxjs';
import {Response} from '../entity/response';
import {environment} from '../../environments/environment';
import {User} from '../entity/user';
import {Page} from '../entity/page';
import {QueryBuilder} from './QueryBuilder';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl: string = environment.baseUrl + '/users';

  constructor(
    private queryBuilder: QueryBuilder,
    private http: HttpClient
  ) {
  }

  public list(term: string, pageNumber: number, sortBy: string, ascending: boolean): Observable<Page<User>> {
    const params: { [k: string]: any } = this.queryBuilder.standard(term, pageNumber, sortBy, ascending);
    return this.http.get<Page<User>>(this.baseUrl, {params});
  }

  public changePassword(user: User, changePassword: ChangePassword): Observable<Response> {
    return this.http.put<Response>(this.baseUrl + '/' + user.id + '/password', changePassword);
  }

}
