import {environment} from '../../environments/environment';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class QueryBuilder {

  private pageSize: number = environment.pageSize;

  public standard(term: string, pageNumber: number = 0, sortBy: string = null, ascending: boolean = null): { [k: string]: any } {
    const params: { [k: string]: any } = {page: pageNumber, size: this.pageSize};
    if (term != null && term.length > 0) {
      params.term = term;
    }
    if (sortBy != null && sortBy.length > 0) {
      params.sort = sortBy + (ascending == null ? '' : (ascending ? ',asc' : ',desc'));
    }
    return params;
  }

  public withoutTerms(pageNumber: number = 0, sortBy: string = null, ascending: boolean = null): { [k: string]: any } {
    return this.standard(null, pageNumber, sortBy, ascending);
  }

}
