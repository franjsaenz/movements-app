import {Injectable} from '@angular/core';
import {ActionSheetController} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class ActionSheetService {

  constructor(
    private actionSheetController: ActionSheetController,
    private translateService: TranslateService
  ) {
  }

  public async presentDelete(
    title: string, subtitle: string, action: string, cancel: string, actionHandler: () => boolean | void | Promise<boolean | void>
  ) {
    await this.present(title, subtitle, action, 'trash', cancel, actionHandler);
  }

  public async presentLeave(
    title: string, subtitle: string, action: string, cancel: string, actionHandler: () => boolean | void | Promise<boolean | void>
  ) {
    await this.present(title, subtitle, action, 'log-out', cancel, actionHandler);
  }

  public async presentUpdate(
    title: string, subtitle: string, action: string, cancel: string, actionHandler: () => boolean | void | Promise<boolean | void>
  ) {
    const translatedSubtitle: string = await this.translateService.get(subtitle).toPromise();
    await this.present(title, translatedSubtitle, action, 'cloud-download', cancel, actionHandler);
  }

  private async present(
    title: string, subtitle: string, action: string, actionIcon: string, cancel: string,
    actionHandler: () => boolean | void | Promise<boolean | void>
  ) {
    const translatedTitle: string = await this.translateService.get(title).toPromise();
    const translatedAction: string = await this.translateService.get(action).toPromise();
    const translatedCancel: string = await this.translateService.get(cancel).toPromise();
    const actionSheet: HTMLIonActionSheetElement = await this.actionSheetController.create({
      header: translatedTitle,
      subHeader: subtitle,
      keyboardClose: false,
      buttons: [{
        text: translatedAction,
        role: 'destructive',
        icon: actionIcon,
        handler: actionHandler
      }, {
        text: translatedCancel,
        icon: 'close',
        role: 'cancel'
      }]
    });
    await actionSheet.present();
  }

}
