import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Sortable} from './sortable';
import {HttpClient} from '@angular/common/http';
import {Account} from '../entity/account';
import {Observable} from 'rxjs';
import {Page} from '../entity/page';
import {Movement} from '../entity/movement';
import {QueryBuilder} from './QueryBuilder';
import {CreateMovement} from '../entity/create-movement';
import {Response} from '../entity/response';

@Injectable({
  providedIn: 'root'
})
export class MovementService {

  private baseUrl: string = environment.baseUrl + '/movements';

  private sortable: Sortable;

  constructor(
    private queryBuilder: QueryBuilder,
    private http: HttpClient
  ) {
    this.sortable = new Sortable({
      sortedBy: null,
      ascending: null,
      elements: [{
        title: 'sort.registered',
        property: 'registered'
      }, {
        title: 'sort.description',
        property: 'description'
      }
        , {
          title: 'sort.debit',
          property: 'debit'
        }, {
          title: 'sort.credit',
          property: 'credit'
        }],
      currentSort: {
        property: 'registered',
        ascending: false
      }
    });
  }

  public getSortable(): Sortable {
    return this.sortable;
  }

  public list(
    account: Account, term: string, pageNumber: number = 0,
    sortBy: string = null, ascending: boolean = null
  ): Observable<Page<Movement>> {
    const params: { [k: string]: any } = this.queryBuilder.standard(term, pageNumber, sortBy, ascending);
    params.accountId = account.id;
    return this.http.get<Page<Movement>>(this.baseUrl, {params});
  }

  public create(account: Account, movement: CreateMovement): Observable<Movement> {
    const params: { [k: string]: any } = {accountId: account.id};
    return this.http.post<Movement>(this.baseUrl, movement, {params});
  }

  public update(movement: Movement, update: CreateMovement): Observable<Movement> {
    return this.http.put<Movement>(this.baseUrl + '/' + movement.id, update);
  }

  public delete(movement: Movement): Observable<Response> {
    return this.http.delete<Response>(this.baseUrl + '/' + movement.id);
  }

}
