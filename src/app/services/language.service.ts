import {Injectable} from '@angular/core';
import {SettingService} from './setting.service';
import {Title} from '@angular/platform-browser';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  private languages: Array<Language>;

  constructor(
    private translateService: TranslateService,
    private settingService: SettingService,
    private title: Title
  ) {
    this.languages = [
      {
        code: 'en',
        name: 'English'
      },
      {
        code: 'es',
        name: 'Español'
      }
    ];
  }

  public async setTitle(key: string = null) {
    if (key == null || key.length < 1) {
      this.title.setTitle('Movements');
    } else {
      const title: string = await this.translateService.get(key).toPromise();
      this.title.setTitle('Movements | ' + title);
    }
  }

  public getLanguages(): Array<Language> {
    return this.languages;
  }

  public async getStored(): Promise<Language> {
    const stored: Language = await this.settingService.getSetting(SettingService.overrideLanguage);
    if (stored == null) {
      return null;
    } else {
      return stored;
    }
  }

  public async useLanguage(lang: Language) {
    let toUse: Language;
    if (lang == null || lang.code == null) {
      toUse = this.languages[0];
    } else {
      toUse = lang;
    }
    await this.settingService.storeSetting(SettingService.overrideLanguage, toUse);
    await this.translateService.use(toUse.code).toPromise();
  }

}

export class Language {
  code: string;
  name: string;
}
