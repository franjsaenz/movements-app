import {Injectable} from '@angular/core';
import {Subject, Subscription} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TabService {

  private observable = new Subject<boolean>();

  constructor() {
  }

  public display(display: boolean) {
    this.observable.next(display);
  }

  public subscribe(sub: (display: boolean) => void): Subscription {
    return this.observable.asObservable().subscribe(sub);
  }

}
