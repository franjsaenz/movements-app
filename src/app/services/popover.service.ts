import {Injectable} from '@angular/core';
import {PopoverController} from '@ionic/angular';
import {SortConfig, SortPopoverComponent} from '../component/sort-popover/sort-popover.component';
import {Subscription} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PopoverService {

  constructor(
    private popoverController: PopoverController
  ) {
  }

  public async presentSort(ev: Event, sortConfig: SortConfig, sortSub: Subscription = null) {
    const popover = await this.popoverController.create({
      component: SortPopoverComponent,
      componentProps: {
        config: sortConfig
      },
      event: ev
    });
    popover.onWillDismiss().then(() => {
      if (sortSub != null) {
        sortSub.unsubscribe();
      }
    });
    await popover.present();
  }

}
