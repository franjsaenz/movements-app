import {Injectable} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {ComponentRef} from '@ionic/core';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(
    private modalController: ModalController
  ) {
  }

  public async presentModal<T extends ComponentRef = ComponentRef>(modalComponent: T, extras: { [k: string]: any } = null) {
    const modal: HTMLIonModalElement = await this.modalController.create({
      component: modalComponent,
      backdropDismiss: false,
      keyboardClose: false,
      componentProps: extras
    });
    await modal.present();
    const response = await modal.onWillDismiss();
    return response == null ? null : response.data;
  }

}
