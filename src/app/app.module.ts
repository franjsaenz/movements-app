import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {TranslateModule} from '@ngx-translate/core';
import {SortPopoverComponent} from './component/sort-popover/sort-popover.component';
import {AuthService} from './services/auth.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CurrencySearchComponent} from './modal/currency-search/currency-search.component';
import {MovementDetailComponent} from './modal/movement-detail/movement-detail.component';
import {CreateAccountComponent} from './modal/create-account/create-account.component';
import {UserSearchComponent} from './modal/user-search/user-search.component';
import {PermissionDetailComponent} from './modal/permission-detail/permission-detail.component';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {AccountSearchComponent} from './modal/account-search/account-search.component';
import {CreateGroupingComponent} from './modal/create-grouping/create-grouping.component';

@NgModule({
  declarations: [
    AppComponent,
    SortPopoverComponent,
    CurrencySearchComponent,
    MovementDetailComponent,
    CreateAccountComponent,
    CreateGroupingComponent,
    UserSearchComponent,
    AccountSearchComponent,
    PermissionDetailComponent
  ],
  entryComponents: [],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule.forRoot(),
    TranslateModule.forRoot({
      defaultLanguage: 'en'
    }),
    ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production})
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
    {provide: HTTP_INTERCEPTORS, useClass: AuthService, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

