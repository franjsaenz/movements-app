import {Component, OnDestroy, OnInit} from '@angular/core';
import {TabService} from '../services/tab.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss']
})
export class TabsPage implements OnInit {

  display = true;

  private subscription: Subscription;

  constructor(
    private tabService: TabService
  ) {
  }

  ngOnInit() {
    this.subscription = this.tabService.subscribe(((display: boolean) => {
      this.display = display;
    }));
  }

}
