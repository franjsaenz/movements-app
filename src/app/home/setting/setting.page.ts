import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {Language, LanguageService} from '../../services/language.service';
import {ActionSheetService} from '../../services/action-sheet.service';
import {AuthService} from '../../services/auth.service';
import {DateFormat, SettingService} from '../../services/setting.service';
import {ModalService} from '../../services/modal.service';
import {Status} from '../../entity/status';
import {ChangePassword} from '../../entity/change-password';
import {Credential} from '../../entity/credential';
import {User} from '../../entity/user';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {SwUpdate} from '@angular/service-worker';
import {TabService} from '../../services/tab.service';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.page.html',
  styleUrls: ['./setting.page.scss']
})
export class SettingPage implements OnInit {

  version: string = environment.version;

  sending = false;

  updating = false;

  user: User;

  passwordForm: FormGroup;

  status: Status;

  now: Date = new Date();

  dateFormats: DateFormat[];

  defaultDateFormat: DateFormat;

  languages: Array<Language>;

  defaultLang: Language;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private errorMappingService: ErrorMappingService,
    private settingService: SettingService,
    private languageService: LanguageService,
    private modalService: ModalService,
    private actionSheetService: ActionSheetService,
    private tabService: TabService,
    private formBuilder: FormBuilder,
    private swUpdate: SwUpdate,
    private router: Router
  ) {
    this.dateFormats = settingService.getDateFormats();
    this.languages = languageService.getLanguages();
  }

  ionViewWillEnter() {
    this.languageService.setTitle('setting.title').then();
    this.tabService.display(true);
  }

  ngOnInit() {
    this.authService.status().subscribe((status: Status) => {
      this.status = status;
      console.log(this.status);
    });
    this.authService.getLoggedIn().then((cred: Credential) => {
      this.user = cred.movementUser;
    });
    this.languageService.getStored().then((defLang: Language) => {
      this.defaultLang = defLang;
    });
    this.settingService.getDefaultDateFormat().then((defaultDateFormat: DateFormat) => {
      this.defaultDateFormat = defaultDateFormat;
    });
    this.initPasswordForm();
  }

  updateDefaultLang(lang: Language) {
    this.defaultLang = null;
    const send: () => void = this.sendingData();
    this.languageService.useLanguage(lang).then(() => {
      setTimeout(() => {
        this.defaultLang = lang;
        send();
      }, 200);
    });
  }

  async refreshPage() {
    await this.actionSheetService.presentUpdate(
      'setting.refreshTitle', 'setting.refreshSubtitle',
      'setting.update', 'general.cancel',
      () => {
        this.refreshPageHandler().then();
      }
    );
  }

  private async refreshPageHandler() {
    this.tabService.display(false);
    this.sending = true;
    this.updating = true;
    if (this.swUpdate.isEnabled) {
      await this.swUpdate.checkForUpdate();
    }
    this.tabService.display(true);
    document.location.reload();
  }

  private initPasswordForm() {
    this.passwordForm = this.formBuilder.group({
      oldPassword: new FormControl('', [
        Validators.required
      ]),
      newPassword: new FormControl('', [
        Validators.required
      ]),
      confirmPassword: new FormControl('', [
        Validators.required
      ])
    }, {
      validators: confirmPasswordValidator('newPassword', 'confirmPassword')
    });
  }

  private sendingData(): () => void {
    this.sending = true;
    this.passwordForm.disable();
    return () => {
      this.sending = false;
      this.passwordForm.reset();
      this.passwordForm.enable();
    };
  }

  async updateDateFormat(format: string) {
    const dateFormat: DateFormat = this.settingService.getDateFormat(format);
    const send: () => void = this.sendingData();
    await this.settingService.storeSetting(SettingService.dateFormatKey, dateFormat);
    setTimeout(() => {
      this.defaultDateFormat = dateFormat;
      send();
    }, 200);
  }

  updatePassword() {
    const pass: ChangePassword = {
      oldPassword: this.passwordForm.get('oldPassword').value,
      newPassword: this.passwordForm.get('newPassword').value
    };
    const send: () => void = this.sendingData();
    this.userService.changePassword(this.user, pass).subscribe(
      () => {
        send();
        this.errorMappingService.displayToast('toast.password', ErrorMappingService.SUCCESS, 2000);
      }, (error: HttpErrorResponse) => {
        send();
        this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
      }
    );
  }

  async logout() {
    await this.authService.logout();
    this.router.navigate(['login'], {replaceUrl: true}).then();
  }

}

export function confirmPasswordValidator(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];
    if (matchingControl.errors && !matchingControl.errors.confirmPasswordValidator) {
      return;
    }
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({confirmPasswordValidator: true});
    } else {
      matchingControl.setErrors(null);
    }
  };
}
