import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {TabsPage} from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'groupings',
        children: [{
          path: '',
          loadChildren: () => import('./grouping/grouping.module').then( m => m.GroupingPageModule)
        }]
      },
      {
        path: 'accounts',
        children: [{
          path: '',
          loadChildren: () => import('./account/account.module').then(m => m.AccountPageModule)
        }]
      },
      {
        path: 'setting',
        children: [{
          path: '',
          loadChildren: () => import('./setting/setting.module').then(m => m.SettingPageModule)
        }]
      },
      {
        path: '',
        redirectTo: '/accounts',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {
}
