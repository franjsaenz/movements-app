import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AccountPage} from './account.page';

const routes: Routes = [
  {
    path: '',
    component: AccountPage
  },
  {
    path: ':accountId',
    loadChildren: () => import('./detail/detail.module').then(m => m.DetailPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountPageRoutingModule {
}
