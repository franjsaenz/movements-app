import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AccountService} from '../../../services/account.service';
import {Account} from '../../../entity/account';
import {ErrorMappingService} from '../../../services/error-mapping.service';
import {HttpErrorResponse} from '@angular/common/http';
import {SortConfig} from '../../../component/sort-popover/sort-popover.component';
import {MovementService} from '../../../services/movement.service';
import {PopoverService} from '../../../services/popover.service';
import {TabService} from '../../../services/tab.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss']
})
export class DetailPage implements OnInit {

  private id: number;

  account: Account;

  segment = 'detail';

  movementSortConfig: SortConfig;

  constructor(
    private accountService: AccountService,
    private movementService: MovementService,
    private errorMappingService: ErrorMappingService,
    private popoverService: PopoverService,
    private tabService: TabService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    const id: any = activatedRoute.snapshot.params.accountId;
    if (id != null) {
      try {
        this.id = +id;
      } catch (e) {
        console.error(e);
        this.id = null;
      }
    }
  }

  ngOnInit() {
    if (this.id == null || isNaN(this.id) || this.id < 1) {
      this.close();
      return;
    }
    this.tabService.display(false);
    this.movementSortConfig = this.movementService.getSortable().getSortConfig();
    this.loadAccount();
  }

  private loadAccount() {
    this.accountService.get(this.id).subscribe((account: Account) => {
      this.account = account;
    }, (error: HttpErrorResponse) => {
      this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING).then();
      this.close();
    });
  }

  close() {
    this.router.navigate(['accounts'], {replaceUrl: true}).then();
  }

  async presentMovementSort(ev: Event) {
    await this.popoverService.presentSort(ev, this.movementSortConfig);
  }

}
