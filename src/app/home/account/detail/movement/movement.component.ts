import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Account} from '../../../../entity/account';
import {LanguageService} from '../../../../services/language.service';
import {IonInfiniteScroll} from '@ionic/angular';
import {Sort, SortConfig} from '../../../../component/sort-popover/sort-popover.component';
import {Movement} from '../../../../entity/movement';
import {MovementService} from '../../../../services/movement.service';
import {Subscription} from 'rxjs';
import {Page} from '../../../../entity/page';
import {HttpErrorResponse} from '@angular/common/http';
import {ErrorMappingService} from '../../../../services/error-mapping.service';
import {ModalService} from '../../../../services/modal.service';
import {MovementDetailComponent} from '../../../../modal/movement-detail/movement-detail.component';
import {DateFormat, SettingService} from '../../../../services/setting.service';

@Component({
  selector: 'app-movement',
  templateUrl: './movement.component.html',
  styleUrls: ['./movement.component.scss']
})
export class MovementComponent implements OnInit, OnDestroy {

  dateFormat: DateFormat;

  @Input() account: Account;

  public loadingContent = true;

  public searchTerm: string;

  public pageNumber = 1;

  public movements: Array<Movement> = [];

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  public sortConfig: SortConfig;

  private sortSub: Subscription;

  constructor(
    private movementService: MovementService,
    private settingService: SettingService,
    private errorMappingService: ErrorMappingService,
    private languageService: LanguageService,
    private modalService: ModalService
  ) {
    settingService.getDefaultDateFormat().then((dateFormat: DateFormat) => {
      this.dateFormat = dateFormat;
    });
  }

  ngOnInit() {
    this.languageService.setTitle('movement.plural').then();
    this.sortConfig = this.movementService.getSortable().getSortConfig();
    this.sortSub = this.movementService.getSortable().subscribeToSort((sort: Sort) => {
      this.sortConfig.currentSort = sort;
      this.sortListing();
    });
    this.listContent();
  }

  ngOnDestroy() {
    this.sortSub.unsubscribe();
  }

  listContent() {
    this.loadingContent = true;
    if (this.infiniteScroll != null) {
      this.infiniteScroll.disabled = true;
    }
    this.movementService.list(
      this.account, this.searchTerm, this.pageNumber - 1,
      this.sortConfig.currentSort.property,
      this.sortConfig.currentSort.ascending
    ).subscribe((resp: Page<Movement>) => {
      this.loadingContent = false;
      this.movements = this.movements.concat(resp.content);
      this.pageNumber++;
      this.infiniteScroll.complete().then();
      this.infiniteScroll.disabled = resp.last;
    }, (error: HttpErrorResponse) => {
      this.loadingContent = false;
      this.infiniteScroll.complete().then();
      this.infiniteScroll.disabled = true;
      this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING).then();
    });
  }

  search(term: string) {
    this.resetList();
    if (term == null || term.length < 1) {
      this.searchTerm = null;
    } else {
      this.searchTerm = term;
    }
    this.listContent();
  }

  resetList() {
    this.searchTerm = null;
    this.movements = [];
    this.pageNumber = 1;
  }

  async open(movement: Movement = null, index: number = null) {
    const response: Movement = await this.modalService.presentModal(MovementDetailComponent, {account: this.account, movement});
    if (response == null || index == null || index < 0) {
      this.movements = [];
      this.pageNumber = 1;
      this.listContent();
    } else {
      this.movements[index] = response;
    }
  }

  sortListing(): void {
    this.movements = [];
    this.pageNumber = 1;
    this.listContent();
  }

}
