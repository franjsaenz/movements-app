import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Account} from '../../../../entity/account';
import {LanguageService} from '../../../../services/language.service';
import {AccountPermission} from '../../../../entity/account-permission';
import {IonInfiniteScroll} from '@ionic/angular';
import {ErrorMappingService} from '../../../../services/error-mapping.service';
import {ModalService} from '../../../../services/modal.service';
import {PermissionService} from '../../../../services/permission.service';
import {Page} from '../../../../entity/page';
import {HttpErrorResponse} from '@angular/common/http';
import {PermissionDetailComponent} from '../../../../modal/permission-detail/permission-detail.component';

@Component({
  selector: 'app-share',
  templateUrl: './share.component.html',
  styleUrls: ['./share.component.scss']
})
export class ShareComponent implements OnInit {

  @Input() account: Account;

  public loadingContent = true;

  public pageNumber = 1;

  permissions: Array<AccountPermission> = [];

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(
    private permissionService: PermissionService,
    private errorMappingService: ErrorMappingService,
    private languageService: LanguageService,
    private modalService: ModalService
  ) {
  }

  ngOnInit() {
    this.languageService.setTitle('account.sharing').then();
    this.listContent();
  }

  listContent() {
    this.loadingContent = true;
    if (this.infiniteScroll != null) {
      this.infiniteScroll.disabled = true;
    }
    this.permissionService.list(
      this.account, this.pageNumber - 1,
      'id.user.name', true
    ).subscribe((resp: Page<AccountPermission>) => {
      this.loadingContent = false;
      this.permissions = this.permissions.concat(resp.content);
      this.pageNumber++;
      this.infiniteScroll.complete().then();
      this.infiniteScroll.disabled = resp.last;
    }, (error: HttpErrorResponse) => {
      this.loadingContent = false;
      this.infiniteScroll.complete().then();
      this.infiniteScroll.disabled = true;
      this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING).then();
    });
  }

  async open(permission: AccountPermission = null, index: number = null) {
    const response: AccountPermission = await this.modalService.presentModal(PermissionDetailComponent, {
      account: this.account,
      permission
    });
    if (response == null || index == null || index < 0) {
      this.permissions = [];
      this.pageNumber = 1;
      this.listContent();
    } else {
      this.permissions[index] = response;
    }
  }

}
