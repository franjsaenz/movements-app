import {Component, Input, OnInit} from '@angular/core';
import {Account} from '../../../../entity/account';
import {LanguageService} from '../../../../services/language.service';
import {Currency} from '../../../../entity/currency';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AccountService} from '../../../../services/account.service';
import {ErrorMappingService} from '../../../../services/error-mapping.service';
import {ActionSheetService} from '../../../../services/action-sheet.service';
import {HttpErrorResponse} from '@angular/common/http';
import {ModalService} from '../../../../services/modal.service';
import {CurrencySearchComponent} from '../../../../modal/currency-search/currency-search.component';
import {Router} from '@angular/router';
import {PermissionService} from '../../../../services/permission.service';
import {AuthService} from '../../../../services/auth.service';
import {Credential} from '../../../../entity/credential';

@Component({
  selector: 'app-account-info',
  templateUrl: './account-info.component.html',
  styleUrls: ['./account-info.component.scss']
})
export class AccountInfoComponent implements OnInit {

  editing = false;

  isSending = false;

  @Input() account: Account;

  selectedCurrency: Currency;

  form: FormGroup;

  constructor(
    private accountService: AccountService,
    private permissionService: PermissionService,
    private authService: AuthService,
    private errorMappingService: ErrorMappingService,
    private actionSheetService: ActionSheetService,
    private modalService: ModalService,
    private languageService: LanguageService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.languageService.setTitle('account.detail').then();
    this.selectedCurrency = this.account.currency;
    this.initForm();
  }

  private initForm() {
    this.form = this.formBuilder.group({
      id: new FormControl(this.account.id),
      name: new FormControl(this.account.name, [Validators.required]),
      currency: new FormControl(this.selectedCurrency.name, [Validators.required])
    });
    this.form.get('id').disable();
    this.form.get('name').disable();
    this.form.get('currency').disable();
  }

  edit() {
    this.editing = true;
    this.form.get('name').enable();
    this.form.get('name').markAsTouched();
    this.form.get('currency').enable();
    this.form.get('currency').markAsTouched();
  }

  cancel() {
    this.editing = false;
    this.selectedCurrency = this.account.currency;
    this.form.get('name').reset(this.account.name);
    this.form.get('name').disable();
    this.form.get('currency').reset(this.selectedCurrency.name);
    this.form.get('currency').disable();
  }

  save() {
    const toSend: Account = {
      name: this.form.get('name').value
    };
    this.isSending = true;
    this.accountService.update(this.account, toSend, this.selectedCurrency).subscribe(
      (account: Account) => {
        this.account = account;
        this.isSending = false;
        this.cancel();
      },
      (error: HttpErrorResponse) => {
        this.isSending = false;
        this.editing = true;
        this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING).then();
      }
    );
  }

  async delete() {
    const handler = () => {
      this.isSending = true;
      this.accountService.delete(this.account).subscribe(
        () => {
          this.close();
        }, (error: HttpErrorResponse) => {
          this.isSending = false;
          this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
        }
      );
    };
    await this.actionSheetService.presentDelete('account.delete', this.account.name,
      'general.delete', 'general.cancel', handler
    );
  }

  async leave() {
    const cred: Credential = await this.authService.getLoggedIn();
    const handler = () => {
      this.isSending = true;
      this.permissionService.revoke(this.account, cred.movementUser).subscribe(
        () => {
          this.close();
        }, (error: HttpErrorResponse) => {
          this.isSending = false;
          this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
        }
      );
    };
    await this.actionSheetService.presentLeave('account.leaveTitle', this.account.name,
      'account.leaveAction', 'general.cancel', handler
    );
  }

  close() {
    this.router.navigate(['accounts'], {replaceUrl: true}).then();
  }

  async searchCurrency() {
    const response = await this.modalService.presentModal(CurrencySearchComponent);
    if (response != null) {
      this.selectedCurrency = response;
      this.form.get('currency').setValue(this.selectedCurrency.name);
    }
  }

}
