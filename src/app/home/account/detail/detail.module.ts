import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailPageRoutingModule } from './detail-routing.module';

import { DetailPage } from './detail.page';
import {TranslateModule} from '@ngx-translate/core';
import {AccountInfoComponent} from './account-info/account-info.component';
import {MovementComponent} from './movement/movement.component';
import {ShareComponent} from './share/share.component';
import {FlexLayoutModule} from '@angular/flex-layout';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailPageRoutingModule,
    TranslateModule,
    ReactiveFormsModule,
    FlexLayoutModule
  ],
  declarations: [DetailPage, AccountInfoComponent, MovementComponent, ShareComponent]
})
export class DetailPageModule {}
