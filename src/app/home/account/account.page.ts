import {Component, OnInit, ViewChild} from '@angular/core';
import {Sort, SortConfig} from '../../component/sort-popover/sort-popover.component';
import {IonInfiniteScroll} from '@ionic/angular';
import {Account} from '../../entity/account';
import {AccountService} from '../../services/account.service';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {ModalService} from '../../services/modal.service';
import {PopoverService} from '../../services/popover.service';
import {LanguageService} from '../../services/language.service';
import {Page} from '../../entity/page';
import {HttpErrorResponse} from '@angular/common/http';
import {Subscription} from 'rxjs';
import {CreateAccountComponent} from '../../modal/create-account/create-account.component';
import {Router} from '@angular/router';
import {TabService} from '../../services/tab.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss']
})
export class AccountPage implements OnInit {

  public loadingContent = true;

  public searchTerm: string;

  public pageNumber = 1;

  public accounts: Array<Account> = [];

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  public sortConfig: SortConfig;

  constructor(
    private accountService: AccountService,
    private errorMappingService: ErrorMappingService,
    private modalService: ModalService,
    private popoverService: PopoverService,
    private languageService: LanguageService,
    private tabService: TabService,
    private router: Router
  ) {
  }

  ionViewWillEnter() {
    this.languageService.setTitle('account.plural').then();
    this.resetList();
    this.listContent();
    this.tabService.display(true);
  }

  ngOnInit() {
    this.sortConfig = this.accountService.getSortable().getSortConfig();
  }

  listContent() {
    this.loadingContent = true;
    if (this.infiniteScroll != null) {
      this.infiniteScroll.disabled = true;
    }
    this.accountService.search(
      this.searchTerm, this.pageNumber - 1,
      this.sortConfig.currentSort.property,
      this.sortConfig.currentSort.ascending
    ).subscribe((resp: Page<Account>) => {
      this.loadingContent = false;
      this.accounts = this.accounts.concat(resp.content);
      this.pageNumber++;
      this.infiniteScroll.complete().then();
      this.infiniteScroll.disabled = resp.last;
    }, (error: HttpErrorResponse) => {
      this.loadingContent = false;
      this.infiniteScroll.complete().then();
      this.infiniteScroll.disabled = true;
      this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING).then();
    });
  }

  search(term: string) {
    this.resetList();
    if (term == null || term.length < 1) {
      this.searchTerm = null;
    } else {
      this.searchTerm = term;
    }
    this.listContent();
  }

  resetList() {
    this.searchTerm = null;
    this.accounts = [];
    this.pageNumber = 1;
  }

  async create() {
    const account: Account = await this.modalService.presentModal(CreateAccountComponent);
    if (account != null) {
      this.router.navigate(['accounts', account.id], {replaceUrl: true}).then();
    }
  }

  async presentSort(ev: Event) {
    const sortSub: Subscription = this.accountService.getSortable().subscribeToSort((sort: Sort) => {
      this.sortConfig.currentSort = sort;
      this.sortListing();
    });
    await this.popoverService.presentSort(ev, this.sortConfig, sortSub);
  }

  sortListing(): void {
    this.accounts = [];
    this.pageNumber = 1;
    this.listContent();
  }

}
