import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GroupingPageRoutingModule } from './grouping-routing.module';

import { GroupingPage } from './grouping.page';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GroupingPageRoutingModule,
    TranslateModule
  ],
  declarations: [GroupingPage]
})
export class GroupingPageModule {}
