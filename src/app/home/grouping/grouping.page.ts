import {Component, OnInit, ViewChild} from '@angular/core';
import {Grouping} from '../../entity/grouping';
import {IonInfiniteScroll} from '@ionic/angular';
import {Sort, SortConfig} from '../../component/sort-popover/sort-popover.component';
import {GroupingService} from '../../services/grouping.service';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {ModalService} from '../../services/modal.service';
import {PopoverService} from '../../services/popover.service';
import {LanguageService} from '../../services/language.service';
import {TabService} from '../../services/tab.service';
import {Router} from '@angular/router';
import {Page} from '../../entity/page';
import {Account} from '../../entity/account';
import {HttpErrorResponse} from '@angular/common/http';
import {Subscription} from 'rxjs';
import {CreateGroupingComponent} from '../../modal/create-grouping/create-grouping.component';

@Component({
  selector: 'app-grouping',
  templateUrl: './grouping.page.html',
  styleUrls: ['./grouping.page.scss']
})
export class GroupingPage implements OnInit {

  public loadingContent = true;

  public searchTerm: string;

  public pageNumber = 1;

  groupings: Array<Grouping> = [];

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  public sortConfig: SortConfig;

  constructor(
    private groupingService: GroupingService,
    private errorMappingService: ErrorMappingService,
    private modalService: ModalService,
    private popoverService: PopoverService,
    private languageService: LanguageService,
    private tabService: TabService,
    private router: Router
  ) {
  }

  ionViewWillEnter() {
    this.languageService.setTitle('grouping.plural').then();
    this.resetList();
    this.listContent();
    this.tabService.display(true);
  }

  ngOnInit() {
    this.sortConfig = this.groupingService.getSortable().getSortConfig();
  }

  listContent() {
    this.loadingContent = true;
    if (this.infiniteScroll != null) {
      this.infiniteScroll.disabled = true;
    }
    this.groupingService.search(
      this.searchTerm, this.pageNumber - 1,
      this.sortConfig.currentSort.property,
      this.sortConfig.currentSort.ascending
    ).subscribe((resp: Page<Account>) => {
      this.loadingContent = false;
      this.groupings = this.groupings.concat(resp.content);
      this.pageNumber++;
      this.infiniteScroll.complete().then();
      this.infiniteScroll.disabled = resp.last;
    }, (error: HttpErrorResponse) => {
      this.loadingContent = false;
      this.infiniteScroll.complete().then();
      this.infiniteScroll.disabled = true;
      this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING).then();
    });
  }

  search(term: string) {
    this.resetList();
    if (term == null || term.length < 1) {
      this.searchTerm = null;
    } else {
      this.searchTerm = term;
    }
    this.listContent();
  }

  resetList() {
    this.searchTerm = null;
    this.groupings = [];
    this.pageNumber = 1;
  }

  async create() {
    const response: Grouping = await this.modalService.presentModal(CreateGroupingComponent);
    if (response != null && response.id > 0) {
      this.router.navigate(['groupings', response.id], {replaceUrl: true}).then();
    }
  }

  async presentSort(ev: Event) {
    const sortSub: Subscription = this.groupingService.getSortable().subscribeToSort((sort: Sort) => {
      this.sortConfig.currentSort = sort;
      this.sortListing();
    });
    await this.popoverService.presentSort(ev, this.sortConfig, sortSub);
  }

  sortListing(): void {
    this.groupings = [];
    this.pageNumber = 1;
    this.listContent();
  }

}
