import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {GroupingPage} from './grouping.page';

const routes: Routes = [
  {
    path: '',
    component: GroupingPage
  },
  {
    path: ':groupingId',
    loadChildren: () => import('./grouping-detail/grouping-detail.module').then(m => m.GroupingDetailPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupingPageRoutingModule {
}
