import {Component, OnInit} from '@angular/core';
import {Grouping} from '../../../entity/grouping';
import {SortConfig} from '../../../component/sort-popover/sort-popover.component';
import {ErrorMappingService} from '../../../services/error-mapping.service';
import {PopoverService} from '../../../services/popover.service';
import {TabService} from '../../../services/tab.service';
import {ActivatedRoute, Router} from '@angular/router';
import {GroupingService} from '../../../services/grouping.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-grouping-detail',
  templateUrl: './grouping-detail.page.html',
  styleUrls: ['./grouping-detail.page.scss']
})
export class GroupingDetailPage implements OnInit {

  private id: number;

  grouping: Grouping;

  segment = 'detail';

  accountSortConfig: SortConfig;

  constructor(
    private groupingService: GroupingService,
    private errorMappingService: ErrorMappingService,
    private popoverService: PopoverService,
    private tabService: TabService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    const id: any = activatedRoute.snapshot.params.groupingId;
    if (id != null) {
      try {
        this.id = +id;
      } catch (e) {
        console.error(e);
        this.id = null;
      }
    }
  }

  ngOnInit() {
    if (this.id == null || isNaN(this.id) || this.id < 1) {
      this.close();
      return;
    }
    this.tabService.display(false);
    this.accountSortConfig = this.groupingService.getElementSortable().getSortConfig();
    this.loadGrouping();
  }

  private loadGrouping() {
    this.groupingService.get(this.id).subscribe((grouping: Grouping) => {
      this.grouping = grouping;
    }, (error: HttpErrorResponse) => {
      this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING).then();
      this.close();
    });
  }

  close() {
    this.router.navigate(['groupings'], {replaceUrl: true}).then();
  }

  async presentMovementSort(ev: Event) {
    await this.popoverService.presentSort(ev, this.accountSortConfig);
  }

}
