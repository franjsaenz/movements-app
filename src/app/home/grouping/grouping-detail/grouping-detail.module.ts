import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {GroupingDetailPageRoutingModule} from './grouping-detail-routing.module';

import {GroupingDetailPage} from './grouping-detail.page';
import {TranslateModule} from '@ngx-translate/core';
import {GroupingInfoComponent} from './grouping-info/grouping-info.component';
import {GroupingAccountsComponent} from './grouping-accounts/grouping-accounts.component';
import {FlexLayoutModule} from '@angular/flex-layout';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GroupingDetailPageRoutingModule,
    TranslateModule,
    ReactiveFormsModule,
    FlexLayoutModule
  ],
  declarations: [GroupingDetailPage, GroupingInfoComponent, GroupingAccountsComponent]
})
export class GroupingDetailPageModule {
}
