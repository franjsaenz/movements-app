import {Component, Input, OnInit} from '@angular/core';
import {Grouping} from '../../../../entity/grouping';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {GroupingService} from '../../../../services/grouping.service';
import {ErrorMappingService} from '../../../../services/error-mapping.service';
import {ActionSheetService} from '../../../../services/action-sheet.service';
import {LanguageService} from '../../../../services/language.service';
import {Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-grouping-info',
  templateUrl: './grouping-info.component.html',
  styleUrls: ['./grouping-info.component.scss']
})
export class GroupingInfoComponent implements OnInit {

  editing = false;

  isSending = false;

  @Input() grouping: Grouping;

  form: FormGroup;

  constructor(
    private groupingService: GroupingService,
    private errorMappingService: ErrorMappingService,
    private actionSheetService: ActionSheetService,
    private languageService: LanguageService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.languageService.setTitle('grouping.detail').then();
    this.initForm();
    this.groupingService.get(this.grouping.id).subscribe(
      (grouping: Grouping) => {
        this.grouping.balances = grouping.balances;
      }
    );
  }

  private initForm() {
    this.form = this.formBuilder.group({
      name: new FormControl(this.grouping.name, [Validators.required])
    });
    this.form.get('name').disable();
  }

  edit() {
    this.editing = true;
    this.form.get('name').enable();
    this.form.get('name').markAsTouched();
  }

  cancel() {
    this.editing = false;
    this.form.get('name').reset(this.grouping.name);
    this.form.get('name').disable();
  }

  save() {
    const toSend: Grouping = {
      name: this.form.get('name').value
    };
    this.isSending = true;
    this.groupingService.update(this.grouping, toSend).subscribe(
      (grouping: Grouping) => {
        this.grouping = grouping;
        this.isSending = false;
        this.cancel();
      },
      (error: HttpErrorResponse) => {
        this.isSending = false;
        this.editing = true;
        this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING).then();
      }
    );
  }

  async delete() {
    const handler = () => {
      this.isSending = true;
      this.groupingService.delete(this.grouping).subscribe(
        () => {
          this.close();
        }, (error: HttpErrorResponse) => {
          this.isSending = false;
          this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
        }
      );
    };
    await this.actionSheetService.presentDelete('grouping.delete', this.grouping.name,
      'general.delete', 'general.cancel', handler
    );
  }

  close() {
    this.router.navigate(['groupings'], {replaceUrl: true}).then();
  }

}
