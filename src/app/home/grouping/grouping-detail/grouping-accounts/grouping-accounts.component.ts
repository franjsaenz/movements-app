import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Grouping} from '../../../../entity/grouping';
import {IonInfiniteScroll} from '@ionic/angular';
import {Account} from '../../../../entity/account';
import {GroupingService} from '../../../../services/grouping.service';
import {ErrorMappingService} from '../../../../services/error-mapping.service';
import {LanguageService} from '../../../../services/language.service';
import {ModalService} from '../../../../services/modal.service';
import {Page} from '../../../../entity/page';
import {HttpErrorResponse} from '@angular/common/http';
import {AccountSearchComponent} from '../../../../modal/account-search/account-search.component';
import {Sort, SortConfig} from '../../../../component/sort-popover/sort-popover.component';
import {Subscription} from 'rxjs';
import {Response} from '../../../../entity/response';

@Component({
  selector: 'app-grouping-accounts',
  templateUrl: './grouping-accounts.component.html',
  styleUrls: ['./grouping-accounts.component.scss']
})
export class GroupingAccountsComponent implements OnInit, OnDestroy {

  @Input() grouping: Grouping;

  public loadingContent = true;

  public pageNumber = 1;

  accounts: Array<Account> = [];

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  public sortConfig: SortConfig;

  private sortSub: Subscription;

  constructor(
    private groupingService: GroupingService,
    private errorMappingService: ErrorMappingService,
    private languageService: LanguageService,
    private modalService: ModalService
  ) {
  }

  ngOnInit() {
    this.languageService.setTitle('movement.plural').then();
    this.sortConfig = this.groupingService.getElementSortable().getSortConfig();
    this.sortSub = this.groupingService.getElementSortable().subscribeToSort((sort: Sort) => {
      this.sortConfig.currentSort = sort;
      this.sortListing();
    });
    this.listContent();
  }

  ngOnDestroy() {
    this.sortSub.unsubscribe();
  }

  listContent() {
    this.loadingContent = true;
    if (this.infiniteScroll != null) {
      this.infiniteScroll.disabled = true;
    }
    this.groupingService.listAccounts(
      this.grouping, this.pageNumber - 1,
      this.sortConfig.currentSort.property,
      this.sortConfig.currentSort.ascending
    ).subscribe((resp: Page<Account>) => {
      this.loadingContent = false;
      this.accounts = this.accounts.concat(resp.content);
      this.pageNumber++;
      this.infiniteScroll.complete().then();
      this.infiniteScroll.disabled = resp.last;
    }, (error: HttpErrorResponse) => {
      this.loadingContent = false;
      this.infiniteScroll.complete().then();
      this.infiniteScroll.disabled = true;
      this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING).then();
    });
  }

  resetList() {
    this.accounts = [];
    this.pageNumber = 1;
  }

  async addAccount() {
    const response: Account = await this.modalService.presentModal(AccountSearchComponent);
    if (response != null && response.id > 0) {
      this.resetList();
      this.loadingContent = true;
      this.groupingService.addAccount(this.grouping, response).subscribe(
        (ignored: Account) => {
          this.listContent();
        }, (error: HttpErrorResponse) => {
          this.loadingContent = false;
          this.infiniteScroll.complete().then();
          this.infiniteScroll.disabled = true;
          this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING).then();
          this.listContent();
        }
      );
    }
  }

  removeAccount(account: Account) {
    if (account == null || account.id < 1) {
      return;
    }
    this.resetList();
    this.loadingContent = true;
    this.groupingService.removeAccount(this.grouping, account).subscribe(
      (ignored: Response) => {
        this.listContent();
      }, (error: HttpErrorResponse) => {
        this.loadingContent = false;
        this.infiniteScroll.complete().then();
        this.infiniteScroll.disabled = true;
        this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING).then();
        this.listContent();
      }
    );
  }

  sortListing(): void {
    this.accounts = [];
    this.pageNumber = 1;
    this.listContent();
  }

}
